#!/bin/bash

if [[ $DEBUG == 'yes' ]]
then
flask run --debug --host=0.0.0.0
else
exec gunicorn -b 0.0.0.0:5000 myapp_start:app
fi
