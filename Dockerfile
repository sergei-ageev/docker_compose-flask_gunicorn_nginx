FROM python:3.10.10-slim-buster

LABEL maintainer="Sergei Ageev"

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV FLASK_APP myapp_start.py
ENV USER appuser
ENV UID 1000
ENV PATH=$PATH:/home/$USER/.local/bin

RUN adduser \
    --disabled-password \
    --gecos "" \
    --shell "/sbin/nologin" \
    --uid "${UID}" \
    "${USER}"

USER "${USER}"

# set work directory
WORKDIR /app

# install dependencies
COPY --chown="${USER}" entrypoint.sh config.py myapp_start.py requirements.txt /app/
RUN pip install -r requirements.txt

ENTRYPOINT ["/app/entrypoint.sh"]
