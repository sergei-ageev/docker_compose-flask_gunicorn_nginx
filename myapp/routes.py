from flask import render_template, flash, redirect, request
from myapp import app, db
from myapp.forms import UserForm
from myapp.models import People

@app.before_first_request
def create_tables():
    db.create_all()

@app.route('/', methods=['GET', 'POST'])
def index():
    form = UserForm()
    if form.validate_on_submit():
        human = People(name=form.name.data, city=form.city.data)
        db.session.add(human)
        db.session.commit()
        flash('Спасибо, что отметились')
        return redirect('/visitors')
    return render_template('index.html', form=form)

@app.route('/visitors')
def visitors():
    visitors = People.query.all()
    return render_template('visitors.html', visitors=visitors)
