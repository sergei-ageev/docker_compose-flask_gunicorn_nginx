from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class UserForm(FlaskForm):
    name = StringField('Ваше имя', validators=[DataRequired()])
    city = StringField('Ваш город', validators=[DataRequired()])
    submit = SubmitField('Отправить')